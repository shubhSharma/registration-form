<?php
include("dbcon.php");
$cityhandler=mysqli_query($dbhandle,"select * from cities");
$statehandler=mysqli_query($dbhandle,"select * from cities");
if(isset($_GET["del_id"])){
  $del_id=$_GET["del_id"];
  mysqli_query($dbhandle,"delete from students where id='$del_id'");
  header("Location:register.php");die();
}
if(isset($_GET["edit_id"])){
  $eid=$_GET["edit_id"];
 $edithandle=mysqli_query($dbhandle,"select * from students where id='$eid'");
  if(mysqli_num_rows($edithandle)>0){
    $editrow=mysqli_fetch_array($edithandle);
        $editid=$editrow["id"];
      $sname=$editrow["full_name"];
      $semail=$editrow["email"];
      $sgender=$editrow["gender"];
      $sstate=$editrow["state"];
      $scity=$editrow["city"];
      $sbranch=$editrow["branch"];
  }
}

for($i=1;$i<=5;$i++)
{
  for($j=5;$j>=$i;$j--)
  {
  echo "*";
  }
  echo "<br>";
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Registration</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
 
</head>
<body>
	<div class="container">
<form>
  <div class="form-group">
    <label for="name">Full Name</label>
    <input type="text" class="form-control" id="name" name="name" value="<?php if(isset($sname)) {echo $sname;} ?>" placeholder="Enter email">
    <input type="hidden" name="id" id="eid" value="<?php echo $editid ?>">
  </div>
 <div class="form-group">
    <label for="mail">Email address</label>
    <input type="email" class="form-control" id="email" name="email" value="<?php if(isset($semail)) { echo $semail;} ?>" placeholder="Enter email">
  </div>
 <div class="form-group">
    <label for="gender">GENDER</label>
    <select class="form-control" id="gender" name="gender">
    	<option value="">select gender</option>
    	<option value="male" <?php if(isset($sgender)){if($sgender=="male"){echo 'selected';}} ?>>Male</option>
    	<option value="female" <?php if(isset($sgender)){if($sgender=="female"){echo 'selected';}} ?>>Female</option>
    	<option value="other" <?php if(isset($sgender)){if($sgender=="other"){echo 'selected';}} ?>>other</option>
    </select>
  </div>
  <div class="form-group">
    <label for="Branch">Branch</label>
    <select class="form-control" id="branch" name="branch">
    	<option value="">select Branch</option>
    	<option value="art" <?php if(isset($sbranch)){if($sbranch=="art"){echo 'selected';}} ?>>Art</option>
    	<option value="science"  <?php if(isset($sbranch)){ if($sbranch=="science"){echo 'selected';}} ?>>Science</option>
    	<option value="commerce"   <?php if(isset($sbranch)){ if($sbranch=="commerce"){echo 'selected';} }?>>commerce</option>
    </select>
  </div>
  <div class="form-group">
    <label for="city">City</label>
    <select name="city" id="city">
      <option value="">Select City</option>
      <?php
        if(mysqli_num_rows($cityhandler)>0){
          while ($city=mysqli_fetch_array($cityhandler)) {
            $cid=$city["city_id"];
            $city=$city["city_name"];
            if($city==$scity){

            echo "<option value='$city' selected>$city</option>";
          }else{
            echo "<option value='$city'>$city</option>";
          }
          }
        }
      ?>
    </select>
  </div>
 <div class="form-group">
    <label for="state">state</label>
    <select name="state" id="state">
      <option value=""> Select State</option>
      <?php 
        if(mysqli_num_rows($statehandler)>0){
       while($state=mysqli_fetch_array($statehandler)) {
          $id=$state["city_id"];
          $cstate=$state["city_state"];
          if($cstate==$sstate){
          echo "<option value='$cstate' selected >$cstate</option>";
        }else{
          echo "<option value='$cstate' >$cstate</option>";
        }
        }
      }
      ?>
    </select>
  </div> 

  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" id="password" value="<?php if(isset($spwd)) {echo $spwd ;}?>" placeholder="Password">
  </div>

  <button type="button" id="submit" class="btn btn-primary">Submit</button>
</form>
<div id="show">
  <?php
$showhandler=mysqli_query($dbhandle,"select * from students");
  if(mysqli_num_rows($showhandler)>0){?>
  <table>
      <tbody>
      <tr>
      <td>Name </td> 
      <td> email</td>
      <td>address</td>
      <td>gender</td>
      <td>Action</td>
      </tr>
  <?php  
    while($row=mysqli_fetch_array($showhandler)){
      $sid=$row["id"];
      $sname=$row["full_name"];
      $semail=$row["email"];
      $sgender=$row["gender"];
      $sstate=$row["state"];
      $scity=$row["city"];
      $scity=$row["branch"];
      ?>
      <tr><td><?php echo $sname; ?></td>
        <td><?php echo $semail; ?></td>
        <td><?php echo $sgender; ?></td>
        <td><?php echo $sstate,$scity; ?></td>
        <td><a href='register.php?edit_id=<?php echo $sid;?>' class='btn btn-primary'>Edit</a></td>
        <td><a href='register.php?del_id=<?php echo $sid;?>' class='btn btn-primary'>del</a></td>
      </tr>
   <?php 
 }?>
    </tbody>
  </table>
   <?php 
 }?>

</div>
</div>
 <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $("#submit").click(function(){
        var name = $("#name").val();
        var edit = $("#eid").val();
        var email = $("#email").val();
        var city = $("#city").val();
        var state = $("#state").val();
        var branch = $("#branch").val();
        var gender = $("#gender").val();
        var pwd = $("#password").val();
        if((name=="") || (email=="") || (city=="") || (state=="") || (branch=="") || (gender=="") || (pwd=="") )
        { 
          alert("Please fill all required filled");
         }else{
        var data = 'n='+ name+'&i='+edit+'&e='+email+'&g='+gender+'&c='+city+'&s='+state+'&pwd='+pwd+'&b='+branch;
              // alert(data);
              $.ajax({
                type:'POST',
                url:'register-handler.php',
                data:data,
                success:function(result){
                  // alert(result);
                  // window.location.href="register.php";
                  $("#show").html(result);
                }
               });
          }

         });
             $("#city").change(function(){
            var city=$(this).val();
              // alert(city);
            if(city!==""){
              var result='c='+city;
            $.ajax({
                type:'POST',
                url:'ajax-onchange-update-state.php',
                data:result,
                success:function(data){
                  // alert(data);
                  $("#state").val(data);
                }
               }
               );
          
          }
          }
          );

    });
  </script>
</body>
</html>